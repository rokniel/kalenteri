import React from 'react';
import format from 'date-fns/format'

const month = (props) => {
    return (      
        <div className='column'>
            <div className="is-pulled-right">{format(props.currentDay,'MMMM')}</div>
        </div>       
    );
}

export default month;