import React from 'react';
import Week from './week/Week';
import Month from './month/Month';
import eachDay from 'date-fns/each_day';
import isFirstDayOfMonth from 'date-fns/is_first_day_of_month'
import subDays from 'date-fns/sub_days'
import addDays from 'date-fns/add_days'

const Calendar = (props) => {

    //Dates between which the calendar will be generated
    const enteredStartDate = new Date(2018,0,27);
    const enteredEndDate = new Date(2018,5,31);
    console.log(enteredStartDate)
    console.log(enteredEndDate)
    //Add function to get week day number starting from monday
    Date.prototype.getISODay = function(){ return (this.getDay() + 6) % 7; }

    //Pad out the selected dates so they start on monday and end on sunday 
    const firstDaysEmpty = enteredStartDate.getISODay();
    console.log(firstDaysEmpty)
    const startDate = subDays(enteredStartDate, firstDaysEmpty);
    const lastDaysEmpty = (7-enteredEndDate.getISODay())-1;
    console.log(lastDaysEmpty)
    const endDate = (addDays(enteredEndDate, lastDaysEmpty))
    console.log(startDate)
    console.log(endDate)
    //Populate an array with all days between selected dates
    const listOfDays = eachDay(startDate, endDate);

    //Split the array in to 7 day chunks (Weeks)
    let weeks = [[]];
    let iWeek = 0;
    for (let i = 0; i < listOfDays.length; i++) {
        listOfDays[i].dayOfWeek = listOfDays[i].getISODay();
        if (isFirstDayOfMonth(listOfDays[i])) {listOfDays[i].firstDayOfMonth = true}
        weeks[iWeek].push(listOfDays[i]);
        if (listOfDays[i].getISODay() === 6) {
            weeks.push([])
            iWeek++;
        }
    }

    //Remove straggling array
    weeks.splice(weeks.length-1,1);


    const calendar = weeks.map((week, weekKey) => {
        let day;
        for (day in week) {
            if (week[day].firstDayOfMonth) {
                return (
                    <div key={weekKey} className='columns'>
                       <Month currentDay={week[day]}/> <Week currentWeek={week}/>
                    </div>
                )
            }
        }
        return (
            
            <div key={weekKey} className='columns'>
                <div className='column'/> <Week currentWeek={week}/>
            </div>
        )
    })

    return (
        <div>
            <h1>Calendar</h1>
            <hr/>
            {calendar}
        </div>
    );
}

export default Calendar;