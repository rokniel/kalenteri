import React from 'react';
import Day from './day/Day';
import getISOWeek from 'date-fns/get_iso_week'

const week = (props) => {
  let _week = [0,1,2,3,4,5,6];

  const row = _week.map((day, dayKey) => {

      return (
        <div className='box is-inline is-radiusless is-unselectable' key={dayKey}>
          <Day date={props.currentWeek[day]}/>
        </div>
      )
    
    
  })
  return (    
    <div className='column'>
      <div><span>{getISOWeek(props.currentWeek[0])}</span>{row}</div>
    </div>    
  );
}

export default week;