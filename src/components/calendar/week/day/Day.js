import React from 'react';

const day = (props) => {
  let date = new Date(props.date);
  date = date.getDate();
  date = date.toString();
    return (
      
        <span>
            {date.padStart(2, "0")}
        </span>
       
    );
}

export default day;